﻿using System;

namespace ShiftArrayElements
{
    public static class Shifter
    {
        public static int[] Shift(int[]? source, int[]? iterations)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations == null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            for (int i = 0; i < iterations.Length; i++)
            {
                for (int c = 0; c < iterations[i]; c++)
                {
                    switch (i % 2)
                    {
                        case 0:
                            {
                                int buf = source[0];
                                for (int j = 0; j < source.Length - 1; j++)
                                {
                                    source[j] = source[j + 1];
                                }

                                source[source.Length - 1] = buf;
                                break;
                            }

                        case 1:
                            {
                                int buf = source[0];
                                for (int j = 0; j < source.Length - 1; j++)
                                {
                                    buf = source[j + 1];
                                    source[j + 1] = source[j];
                                }

                                source[0] = buf;
                                break;
                            }

                        default:
                            throw new InvalidOperationException($"Incorrect {iterations} enum value.");
                    }
                }
            }

            return source;
        }
    }
}
